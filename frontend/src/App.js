import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './ui/layout/Header';
import Footer from './ui/layout/Footer';

import Home from './pages/Home';
import Product from './pages/Product';
import Cart from './pages/Cart';
import Login from './pages/Login';
import Register from './pages/Register';
import Profile from './pages/Profile';
import Shipping from './pages/Shipping';
import Payment from './pages/Payment';
import PlaceOrder from './pages/PlaceOrder';
import Order from './pages/Order';
import UserList from './pages/UserList';
import UserEdit from './pages/UserEdit';
import ProductList from './pages/ProductList';
import ProductEdit from './pages/ProductEdit';
import OrderList from './pages/OrderList';

function App() {
  return (
    <Router>
      <Header />
        <main className='pb-3 pt-3 px-5 main-container'>
          <Container className="main-container-content">
            <Routes>
              <Route path="/" element={ <Home /> } />
              <Route path="/login" element={ <Login /> } />
              <Route path="/register" element={ <Register /> } />
              <Route path="/profile" element={ <Profile /> } />
              <Route path="/shipping" element={ <Shipping /> } />
              <Route path="/payment" element={ <Payment /> } />
              <Route path="/placeorder" element={ <PlaceOrder /> } />
              <Route path="/order/:id" element={ <Order /> } />
              <Route path="/product/:id" element={ <Product /> } />
              <Route path="/cart" element={ <Cart /> } />
              <Route path="/cart/:id" element={ <Cart /> } />
              
              <Route path="/admin/userlist" element={ <UserList /> } />
              <Route path="/admin/user/:id/edit" element={ <UserEdit /> } />

              <Route path="/admin/productlist" element={ <ProductList /> } />
              <Route path="/admin/product/:id/edit" element={ <ProductEdit /> } />
              
              <Route path="/admin/orderlist" element={ <OrderList /> } />
            </Routes>
          </Container>
        </main>
      <Footer />
    </Router>
  );
}

export default App;
