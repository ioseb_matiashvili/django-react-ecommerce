import { useEffect } from 'react'
import { Row, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { listProducts } from '../redux/actions/productActions';

import Product from '../ui/components/Product';
import Loader from '../ui/components/Loader';
import Message from '../ui/components/Message';
import Paginate from '../ui/components/Paginate';
import { useLocation } from 'react-router-dom';
import ProductCarousel from '../ui/components/ProductCarousel';
//import products from '../products';


function Home() {
  const dispatch = useDispatch();
  const location = useLocation();
  const productList = useSelector(state => state.productList);
  const { error, loading, products, page, pages } = productList;

  let keyword = location.search;

  useEffect(() => {
    dispatch(listProducts(keyword))
  }, [dispatch, keyword])

  return (
      <div>
        
        {
          !keyword && <ProductCarousel />
        }
        <h1>Latest Products</h1>

        { loading ? <Loader /> : error ? <Message variant="danger">{error}</Message> : 
          <div>
            <Row>
              {
                products.map(product => (
                  <Col key={product._id} sm={12} md={6} lg={4} xl={3}> 
                    <Product product={product} />
                  </Col>
                ))
              }
            </Row>

              <Paginate page={page} pages={pages} keyword={keyword} />
          </div>
        }

      </div>
  )
}

export default Home;
