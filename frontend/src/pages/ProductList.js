import { useEffect } from 'react';
import { LinkContainer } from 'react-router-bootstrap';
import { Row, Col, Table, Button, } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { listProducts, createProduct, deleteProduct } from '../redux/actions/productActions';
import { PRODUCT_CREATE_RESET } from '../redux/constants/productConstants';
import { useLocation, useNavigate } from 'react-router-dom';
import Loader from '../ui/components/Loader';
import Message from '../ui/components/Message';
import Paginate from '../ui/components/Paginate';

function ProductList() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();

    const productList = useSelector(state => state.productList);
    const {
        loading,
        error,
        products,
        page,
        pages
    } = productList;

    const productDelete = useSelector(state => state.productDelete);
    const {
        loading: loadingDelete,
        error: errorDelete,
        success: successDelete
    } = productDelete;

    const productCreate = useSelector(state => state.productCreate);
    const {
        loading: loadingCreateProduct,
        error: errorCreateProduct,
        success: successProductCreate,
        product: createdProduct
    } = productCreate;

    const userLogin = useSelector(state => state.userLogin);
    const { userInfo } = userLogin;
    let keyword = location.search;

    useEffect(() => {
        dispatch({
            type: PRODUCT_CREATE_RESET
        })
        if (!userInfo.isAdmin) {
            navigate('/login');
        }

        if (successProductCreate) {
            navigate(`/admin/product/${createdProduct?._id}/edit`);
        }
        else {
            dispatch(listProducts(keyword));
        }
    }, [
        dispatch, 
        navigate, 
        userInfo, 
        successProductCreate, 
        createdProduct, 
        successDelete, 
        keyword
    ])

    const handleDeleteProduct = (id) => {
        if (window.confirm('Are you sure you want to delete this Product?')) {
            dispatch(deleteProduct(id));
        }
    }

    const createProductHandler = () => {
        // create product
        dispatch(createProduct());
    }

    return (
        <div>
            <Row className='align-items-center'>
                <Col>
                    <h1>Products</h1>
                </Col>

                <Col className='text-end'>
                    <Button className='my-3' onClick={createProductHandler}>
                        <i className='fas fa-plus'></i> Create Product
                    </Button>
                </Col>
            </Row>

            { loadingDelete && <Loader /> }
            { errorDelete && <Message variant='danger'>{errorDelete}</Message> }

            { loadingCreateProduct && <Loader /> }
            { errorCreateProduct && <Message variant='danger'>{errorCreateProduct}</Message> }

            { loading ? <Loader /> : error ? 
              <Message variant='alert'>{error}</Message> : 
            (
                <div>
                    <Table striped bordered hover responsive className='table-sm'>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Category</th>
                                <th>Brand</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            { products.map(product => (
                                <tr key={product._id}>
                                    <td>{ product._id }</td>
                                    <td>{ product.name }</td>
                                    <td>${ product.price }</td>
                                    <td>{ product.category }</td>
                                    <td>{ product.brand }</td>

                                    <td>
                                        <LinkContainer to={`/admin/product/${product._id}/edit`}>
                                            <Button variant='light' className='btn-sm'>
                                                Edit <i className='fas fa-edit'></i>
                                            </Button>
                                        </LinkContainer>

                                        <Button variant='danger' className='btn-sm' onClick={() => handleDeleteProduct(product._id)}>
                                            Delete <i className='fas fa-trash'></i>
                                        </Button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>

                    <Paginate page={page} pages={pages} isAdmin={true} />
                </div>
                
            )}
        </div>
    )
}

export default ProductList;