import { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate, useParams } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import Loader from '../ui/components/Loader';
import Message from '../ui/components/Message';
import { useSelector, useDispatch } from 'react-redux';
import { getUserDetails, updateUser } from '../redux/actions/userActions';
import FormContainer from '../ui/components/FormContainer';
import { USER_UPDATE_RESET } from '../redux/constants/userConstants';

function EditUser() {
    const location = useLocation();
    const navigate = useNavigate();
    const params = useParams();

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [isAdmin, setIsAdmin] = useState(false);

    const userId = params.id;

    const dispatch = useDispatch();
    const userDetails = useSelector(state => state.userDetails);

    const { 
        error,
        loading,
        user
    } = userDetails;

    const userUpdate = useSelector(state => state.userUpdate);

    const { 
        error: updateError,
        loading: updateLoading,
        success: updateSuccess
    } = userUpdate;

    useEffect(() => {

        if (updateSuccess) {
            dispatch({
                type: USER_UPDATE_RESET
            });
            navigate('/admin/userlist');
        }
        else {
            if (!user.name || user._id !== Number(userId)) {
                dispatch(getUserDetails(userId));
            }
            else {
                setName(user.name);
                setEmail(user.email);
                setIsAdmin(user.isAdmin);
            }
        }

    }, [userId, user, dispatch, navigate, updateSuccess])

    function submitHandler(e) {
        e.preventDefault();

        dispatch(updateUser({
            _id: user._id,
            name,
            email,
            isAdmin
        }));
    }

  return (
      <div>
        <Link to='/admin/userlist'>
            Go Back
        </Link>
        
        <FormContainer>
            <h1>Edit User</h1>

            { updateLoading && <Loader /> }
            { updateError && <Message variant='danger'>{updateError}</Message> }
            { loading ? <Loader /> : error ? <Message variant='danger'>{error}</Message> : 
            (
                <Form onSubmit={submitHandler}>
                    <Form.Group controlId="name">
                        <Form.Label>
                            Name
                        </Form.Label>

                        <Form.Control
                            type="name"
                            placeholder="Enter name"
                            value={ name }
                            onChange={(e) => setName(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group controlId="email">
                        <Form.Label className='mt-3'>
                            Email Address
                        </Form.Label>

                        <Form.Control
                            type="email"
                            placeholder="Enter email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group controlId="isadmin">
                        <Form.Check
                            type="checkbox"
                            label="Is Admin"
                            checked={isAdmin}
                            className='mt-3'
                            onChange={(e) => setIsAdmin(e.target.checked)}
                        />
                    </Form.Group>

                    <Button
                        type="submit"
                        variant="primary"
                        className="mt-3"
                    >
                        Update
                    </Button>
                </Form>
            )}
        </FormContainer>
      </div>
  )
}

export default EditUser;