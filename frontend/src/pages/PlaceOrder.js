import { useEffect } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { Row,Col, ListGroup, Image, Card, Button } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import CheckoutSteps from '../ui/components/CheckoutSteps';
import Message from '../ui/components/Message';
import { createOrder } from '../redux/actions/orderActions'
import { ORDER_CREATE_RESET } from '../redux/constants/orderConstants'

function PlaceOrder() {
    const cart = useSelector(state => state.cart);
    const orderCreate = useSelector(state => state.orderCreate);
    const {
        order,
        success,
        error
    } = orderCreate;
    
    const dispatch = useDispatch();
    const navigate = useNavigate();

    cart.itemsPrice = cart.cartItems.reduce((acc, item) => (
        acc + item.price * item.quantity
    ), 0).toFixed(2);

    cart.shippingPrice = (cart.itemsPrice > 100 ? 0 : 10).toFixed(2);
    cart.taxPrice = Number((0.082) * cart.itemsPrice ).toFixed(2);
    cart.totalPrice = (Number(cart.itemsPrice) + Number(cart.shippingPrice) + Number(cart.taxPrice)).toFixed(2);

    useEffect(() => {
        if(!cart.paymentMethod) {
            navigate('/payment');
        }

        if (success) {
            navigate(`/order/${order._id}`);
            dispatch({
                type: ORDER_CREATE_RESET
            })
        }

    }, [success, navigate, order, cart.paymentMethod, dispatch])

    function placeOrderHandler() {
        const order = {
            orderItems: cart.cartItems,
            shippingAddress: cart.shippingAddress,
            paymentMethod: cart.paymentMethod,
            itemPrice: cart.itemsPrice,
            shippingPrice: cart.shippingPrice,
            taxPrice: cart.taxPrice,
            totalPrice: cart.totalPrice
        }
        console.log(order);
        dispatch(createOrder(order));
    }

    return (
        <div>
            <CheckoutSteps step1 step2 step3 step4 />

            <Row>
                <Col md={8}>
                    <ListGroup variant='flush'>
                        <ListGroup.Item>
                            <h2>Shipping</h2>

                            <p>
                                <strong>Shipping: </strong>
                                { cart.shippingAddress.address }, { cart.shippingAddress.city }
                                { ' ' }
                                { cart.shippingAddress.postalCode }, 
                                { '  ' }
                                { cart.shippingAddress.country }
                            </p>
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <h2>Payment Method</h2>

                            <p>
                                <strong>Method: </strong>

                                { cart.paymentMethod }
                            </p>
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <h2>Order Items</h2>

                            { 
                                cart.cartItems.length === 0 ? <Message variant='info'>Your cart is empty</Message> :
                                <ListGroup variant='flush'>
                                    { cart.cartItems.map((item, index) => (
                                        <ListGroup.Item key={index}>
                                            <Row>
                                                <Col md={1}>
                                                    <Image src={item.image} alt={item.name} fluid rounded />
                                                </Col>

                                                <Col>
                                                    <Link to={`/product/${item.productId}`}>{ item.name }</Link>
                                                </Col>

                                                <Col md={4}>
                                                    { item.quantity } X ${item.price} = ${ (item.quantity * item.price).toFixed(2) }
                                                </Col>
                                            </Row>
                                        </ListGroup.Item> 
                                    )) }
                                </ListGroup>
                            }
                        </ListGroup.Item>
                    </ListGroup>
                </Col>

                <Col md={4}>
                    <Card>
                        <ListGroup variant='flush'>
                            <ListGroup.Item>
                                <h2>Order Summary</h2>
                            </ListGroup.Item>

                            <ListGroup.Item>
                                <Row>
                                    <Col>Items: </Col>
                                    <Col>${cart.itemsPrice}</Col>
                                </Row>
                            </ListGroup.Item>

                            <ListGroup.Item>
                                <Row>
                                    <Col>Shipping: </Col>
                                    <Col>${cart.shippingPrice}</Col>
                                </Row>
                            </ListGroup.Item>

                            <ListGroup.Item>
                                <Row>
                                    <Col>Tax: </Col>
                                    <Col>${cart.taxPrice}</Col>
                                </Row>
                            </ListGroup.Item>

                            <ListGroup.Item>
                                <Row>
                                    <Col>Total Price: </Col>
                                    <Col>${cart.totalPrice}</Col>
                                </Row>
                            </ListGroup.Item>

                            <ListGroup.Item>
                                { error && <Message variant='danger'>{ error }</Message> }
                            </ListGroup.Item>

                            <ListGroup.Item>
                                <Row>
                                    <Button 
                                        type='button' 
                                        disabled = { cart.cartItems === 0 }
                                        onClick = { placeOrderHandler }
                                    >
                                        Place Order
                                    </Button>
                                </Row>
                            </ListGroup.Item>

                        </ListGroup>
                    </Card>
                </Col>
            </Row>
        </div>
    )
}

export default PlaceOrder