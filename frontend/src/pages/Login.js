import { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { Row, Col, Form, Button, } from 'react-bootstrap';
import Loader from '../ui/components/Loader';
import Message from '../ui/components/Message';
import { useSelector, useDispatch } from 'react-redux';
import { login } from '../redux/actions/userActions';
import FormContainer from '../ui/components/FormContainer';

const Login = () => {
    const location = useLocation();
    const navigate = useNavigate();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const redirect = location.search ? location.search.split('=')[1] : '/';

    const dispatch = useDispatch();
    const userLogin = useSelector(state => state.userLogin);
    const { 
        error,
        loading,
        userInfo
    } = userLogin;

    useEffect(() => {
        if (userInfo) {
            navigate(redirect);
        }
    }, [navigate, userInfo, redirect])

    function submitHandler(e) {
        e.preventDefault();
        dispatch(login(email, password));
    }

    return (
        <FormContainer>
            <h1>Sign in</h1>
            
            { error && <Message variant="danger">{ error }</Message> }
            { loading && <Loader /> }

            <Form onSubmit={submitHandler}>
                <Form.Group controlId="email">
                    <Form.Label>
                        Email Address
                    </Form.Label>

                    <Form.Control
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>
                        Password
                    </Form.Label>

                    <Form.Control
                        type="password"
                        placeholder="Enter password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </Form.Group>

                <Button
                    type="submit"
                    variant="primary"
                    className="mt-3"
                >
                    Sign In
                </Button>
            </Form>

            <Row className="py-3">
                <Col>
                    New Customer? <Link to={ redirect ? `/register?redirect=${redirect}` : '/register'}>Register</Link>
                </Col>
            </Row>
        </FormContainer>
    )
}

export default Login