import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Form, Button, Col } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import FormContainer from '../ui/components/FormContainer';
import CheckoutSteps from '../ui/components/CheckoutSteps';
import { savePaymentMethod } from '../redux/actions/cartActions';

function Payment() {
    const navigate = useNavigate();

    const dispatch = useDispatch();
    const [paymentMethod, setPaymentMethod] =useState('PayPal'); 

    const cart = useSelector(state => state.cart);
    const {
        shippingAddress
    } = cart;

    if (!shippingAddress.address) {
        navigate('/shipping');
    }

    function submitHandler(e) {
        e.preventDefault();
        dispatch(savePaymentMethod(paymentMethod));
        navigate('/placeorder');
    }

    return (
        <FormContainer className=''>
            <CheckoutSteps step1 step2 step3 />

            <Form onSubmit={submitHandler}>
                <Form.Group>
                    <Form.Label as='legend'>Select Method</Form.Label>
                    <Col>
                        <Form.Check 
                            type='radio'
                            label='PayPal or Credit Card'
                            id='paypal'
                            name='paymentMethod'
                            checked
                            onChange={(e) => setPaymentMethod(e.target.value)}
                        />
                    </Col>
                </Form.Group>   

                <Button type='submit' variant='primary'>
                    Continue
                </Button>
            </Form>
        </FormContainer>
    )
}

export default Payment