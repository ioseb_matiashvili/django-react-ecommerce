import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Form, Button, } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import FormContainer from '../ui/components/FormContainer';
import CheckoutSteps from '../ui/components/CheckoutSteps';
import { saveShippingAddress } from '../redux/actions/cartActions';

const Shipping = () => {
    const navigate = useNavigate();

    const dispatch = useDispatch();
    const cart = useSelector(state => state.cart);
    const {
        shippingAddress
    } = cart;
    
    const [address, setAddress] = useState(shippingAddress.address);
    const [city, setCity] = useState(shippingAddress.city);
    const [postalCode, setPostalCode] = useState(shippingAddress.postalCode);
    const [country, setCountry] = useState(shippingAddress.country);

    function submitHandler(e) {
        e.preventDefault();
        dispatch(saveShippingAddress({ address, city, postalCode, country }));
        navigate('/payment');
    }

    return (
        <FormContainer>
            <CheckoutSteps step1 step2 />
            <h1>Shipping</h1>
            <Form onSubmit={submitHandler}>
                <Form.Group controlId="address">
                    <Form.Label>
                        Address
                    </Form.Label>

                    <Form.Control
                        type="text"
                        required
                        placeholder="Enter address"
                        value={ address ? address : ''}
                        onChange={(e) => setAddress(e.target.value)}
                    />
                </Form.Group>

                <Form.Group controlId="city">
                    <Form.Label>
                        City
                    </Form.Label>

                    <Form.Control
                        type="text"
                        required
                        placeholder="Enter city"
                        value={ city ? city : ''}
                        onChange={(e) => setCity(e.target.value)}
                    />
                </Form.Group>

                <Form.Group controlId="postalCode">
                    <Form.Label>
                        Postal code
                    </Form.Label>

                    <Form.Control
                        type="text"
                        required
                        placeholder="Enter postal code"
                        value={ postalCode ? postalCode : ''}
                        onChange={(e) => setPostalCode(e.target.value)}
                    />
                </Form.Group>

                <Form.Group controlId="country">
                    <Form.Label>
                        Country
                    </Form.Label>

                    <Form.Control
                        type="text"
                        required
                        placeholder="Enter country"
                        value={ country ? country : ''}
                        onChange={(e) => setCountry(e.target.value)}
                    />
                </Form.Group>

                <Button type='submit' variant='primary' className='mt-3'>
                    Continue
                </Button>
            </Form>
        </FormContainer>
    )
}

export default Shipping