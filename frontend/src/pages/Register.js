import { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { Row, Col, Form, Button, } from 'react-bootstrap';
import Loader from '../ui/components/Loader';
import Message from '../ui/components/Message';
import { useSelector, useDispatch } from 'react-redux';
import { register } from '../redux/actions/userActions';
import FormContainer from '../ui/components/FormContainer';

function Register() {
    const location = useLocation();
    const navigate = useNavigate();

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [message, setMessage] = useState('');

    const redirect = location.search ? location.search.split('=')[1] : '/';

    const dispatch = useDispatch();
    const userRegister = useSelector(state => state.userRegister);

    const { 
        error,
        loading,
        userInfo
    } = userRegister;

    useEffect(() => {
        if (userInfo) {
            navigate(redirect);
        }
    }, [navigate, userInfo, redirect])

    function submitHandler(e) {
        e.preventDefault();

        if (password !== confirmPassword) {
            setMessage('Passwords do not match');
        }
        else {
            dispatch(register(name, email, password));
        }
    }

  return (
    <FormContainer>
        <h1>Register</h1>
        
        { message && <Message variant="danger">{ message }</Message> }
        { error && <Message variant="danger">{ error }</Message> }
        { loading && <Loader /> }

        <Form onSubmit={submitHandler}>
            <Form.Group controlId="name">
                <Form.Label>
                    Name
                </Form.Label>

                <Form.Control
                    type="name"
                    required
                    placeholder="Enter name"
                    value={ name }
                    onChange={(e) => setName(e.target.value)}
                />
            </Form.Group>

            <Form.Group controlId="email">
                <Form.Label>
                    Email Address
                </Form.Label>

                <Form.Control
                    type="email"
                    required
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>
                    Password
                </Form.Label>

                <Form.Control
                    type="password"
                    required
                    placeholder="Enter password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
            </Form.Group>

            <Form.Group controlId="passwordConfirm">
                <Form.Label>
                    Confirm Password
                </Form.Label>

                <Form.Control
                    type="password"
                    required
                    placeholder="Confirm password"
                    value={ confirmPassword }
                    onChange={(e) => setConfirmPassword(e.target.value)}
                />
            </Form.Group>

            <Button
                type="submit"
                variant="primary"
                className="mt-3"
            >
                Register
            </Button>
        </Form>

        <Row className="py-3">
            <Col>
                Have an account? <Link to={ redirect ? `/login?redirect=${redirect}` : '/login'}>Sign In</Link>
            </Col>
        </Row>
    </FormContainer>
  )
}

export default Register