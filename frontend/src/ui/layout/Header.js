import { useDispatch, useSelector } from 'react-redux';
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { logout } from '../../redux/actions/userActions'; 
import Search from '../components/Search';

const Header = () => {
  const userLogin = useSelector(state => state.userLogin);
  const { userInfo } = userLogin;
  const dispatch = useDispatch();

  const cart = useSelector(state => state.cart);
  const { cartItems } = cart;

  function logoutHandler() {
    dispatch(logout());
  }

  return (
      <header>
          <Navbar bg="dark" variant="dark" expand="lg" collapseOnSelect>
            <Container>
              <LinkContainer to="/">
                <Navbar.Brand>Real Shop</Navbar.Brand>
              </LinkContainer>

              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">

                <Search />

                <Nav className="cart-container">
                  <div className="cart-item">
                    <LinkContainer to="/cart">
                      <Nav.Link><i className='fas fa-shopping-cart'></i> Cart</Nav.Link>
                    </LinkContainer>
                    { cartItems.length >= 1 && (
                      <span className="cart-item-number">{ cartItems.length >= 1 && cartItems.length }</span>
                    )}
                  </div>
                </Nav>
                
                <Nav>
                { userInfo && userInfo.isAdmin ? (
                    <NavDropdown title='Admin' id='adminmenu'>
                      <LinkContainer to="/admin/userlist">
                        <NavDropdown.Item>Users</NavDropdown.Item>
                      </LinkContainer>

                      <LinkContainer to="/admin/productlist">
                        <NavDropdown.Item>Products</NavDropdown.Item>
                      </LinkContainer>

                      <LinkContainer to="/admin/orderlist">
                        <NavDropdown.Item>Orders</NavDropdown.Item>
                      </LinkContainer>
                    </NavDropdown>
                  ): null}
                </Nav>

                <Nav>
                  { 
                    userInfo ? (
                      <NavDropdown title={userInfo.name} id='username'>
                        <LinkContainer to="/profile">
                          <NavDropdown.Item>Profile</NavDropdown.Item>
                        </LinkContainer>

                        <NavDropdown.Item onClick={logoutHandler}>Logout</NavDropdown.Item>
                      </NavDropdown>
                    ) : (
                      <LinkContainer to="/login">
                        <Nav.Link><i className='fas fa-user'></i>Login</Nav.Link>
                      </LinkContainer>
                    ) 
                  }
                </Nav>

              </Navbar.Collapse>

            </Container>
          </Navbar>
      </header>
  )
};

export default Header;
