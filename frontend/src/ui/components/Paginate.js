import React from 'react'
import { Pagination } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

const Paginate = ({ page, pages, keyword='', isAdmin=false }) => {

    if (keyword) {
        keyword = keyword.split('?keyword=')[1].split("&")[0];
    }

    console.log(isAdmin)

    return (
        pages > 1 && (
            <Pagination className="d-flex justify-content-center">
                {
                    [...Array(pages).keys()].map((index) => (
                        <LinkContainer 
                            key={index + 1} 
                            to={
                                !isAdmin ?
                                `/?keyword=${keyword}&page=${index + 1}`:
                                `/admin/productlist/?keyword=${keyword}&page=${index + 1}`
                            }
                        >
                            <Pagination.Item active={index + 1 === page}>
                                { index + 1 }
                            </Pagination.Item>
                        </LinkContainer>
                    ))
                }
            </Pagination>
        )
    )
}

export default Paginate