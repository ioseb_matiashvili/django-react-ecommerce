import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { productListReducer, productCreateReducer, productUpdateReducer } from './reducers/ProductReducer'
import { 
    productDetailsReducer, 
    productDeleteReducer,
    productCreateReviewReducer,
    productTopRatedReducer
} from './reducers/ProductReducer'
import { cartReducer } from './reducers/CartReducers'
import { userLoginReducer, 
    userRegisterReducer, 
    userDetailsReducer, 
    userUpdateProfileReducer,
    userListReducer,
    userDeleteReducer,
    userUpdateReducer
} from './reducers/UserReducers';
import {  
    orderCreateReducer, 
    orderDetailsReducer, 
    orderPayReducer, 
    listMyOrdersReducer,
    orderListReducer,
    orderDeliverReducer
} from './reducers/OrderReducer'

const reducer = combineReducers({
    productList: productListReducer,
    productDetails: productDetailsReducer,
    productDelete: productDeleteReducer,
    productCreate: productCreateReducer,
    productUpdate: productUpdateReducer,
    productReviewCreate: productCreateReviewReducer,
    productTopRated: productTopRatedReducer,

    cart: cartReducer,

    userLogin: userLoginReducer,
    userRegister: userRegisterReducer,
    userDetails: userDetailsReducer,
    userUpdateProfile: userUpdateProfileReducer,

    orderCreate: orderCreateReducer,
    orderDetails: orderDetailsReducer,
    orderPay: orderPayReducer,
    orderList: orderListReducer,
    orderDeliver: orderDeliverReducer, 

    listMyOrder: listMyOrdersReducer,
    listAllUsers: userListReducer,
    
    userDelete: userDeleteReducer,
    userUpdate: userUpdateReducer
});

const cartItemsLocal = localStorage.getItem('cartItems') ? JSON.parse(localStorage.getItem('cartItems')) : []
const userInfoLocal = localStorage.getItem('userInfo') ? JSON.parse(localStorage.getItem('userInfo')) : null
const shippingAddressLocal = localStorage.getItem('shippingAddress') ? JSON.parse(localStorage.getItem('shippingAddress')) : {}

const initialState = {
    cart: { 
        cartItems: cartItemsLocal,
        shippingAddress: shippingAddressLocal
    },
    userLogin: { 
        userInfo: userInfoLocal 
    }
};

const middleware = [thunk];

const store = createStore(reducer, initialState, composeWithDevTools( 
                          applyMiddleware(...middleware)));

export default store;